package com.zuitt.example;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Scanner;

public class Activity {

    public static void main(String[] args){

        HashMap<String, Integer> games = new HashMap<>();

        games.put("Mario Odyssey", 50);
        games.put("Super Smash Bros. Ultimate", 20);
        games.put("Luigi's Mansion 3", 15);
        games.put("Pokemon Sword", 30);
        games.put("Pokemon Shield", 100);

        // for checking stocks
        games.forEach((game, stock) -> {
            if(stock > 1) System.out.println(game + " has " + stock + " stocks left.");
            else System.out.println(game + " has " + stock + " stock left.");
        });

        // games are added to topGames if it has less than or equal to 30 stocks left
        ArrayList<String> topGames = new ArrayList<>();

        games.forEach((game, stock) -> {
            if(stock <= 30){
                topGames.add(game);
                System.out.println(game + " has been added to top games list!");
            }
        });

        // print top games
        System.out.println("Our shop's top games:");
        System.out.println(topGames);

        // stretch goal

        Scanner input = new Scanner(System.in);

        boolean addItem = true;

        while(addItem){
            System.out.println("Would you like to add an item? Yes or No.");

            try {
                String res = input.nextLine();
                switch(res.toLowerCase()){
                    case "yes":
                        System.out.println("Add Item Name:");
                        String game = input.nextLine();

                        System.out.println("Add Number of Stocks:");
                        int stock = input.nextInt();

                        games.put(game, stock);

                        if(stock>1) System.out.println(game + " has been added with " + stock + " stocks.");
                        else System.out.println(game + " has been added with " + stock + " stock.");

                        System.out.println("Our list of games: " + games.keySet());

                        break;

                    case "no":
                        System.out.println("Thank you.");
                        addItem = false;

                        break;

                    default:
                        System.out.println("Invalid input. Try again");
                }


            } catch(Exception e) {
                System.out.println("Invalid input. Try again");
                e.printStackTrace();
            }

        }

    }
}
